# WordPress WebDelivery R_Keeper integration (in development)

- Contributors: MrSwed
- Tags: wordpress, plugin, template
- Requires at least: 5.3
- Tested up to: 5.3
- Stable tag: 1.0
- License: GPLv2 or later
- License URI: http://www.gnu.org/licenses/gpl-2.0.html
- Url: https://gitlab.com/sdwp/plugins/wp-r-keeper
- Bug Tracker: https://gitlab.com/sdwp/plugins/wp-r-keeper/issues  

WordPress WebDelivery R_Keeper integration

## Description

```
/ todo: add more description
```


### Capabilities

- getting a list of restaurants, choosing a restaurant to work with the site
- synchronization of dishes and menu of the selected restaurant
- automatic relate of images, during their loading, to dishes, according to a given mask of the file containing the article of the dish

#### Expected

- _sending an order to r_keeper server (in development)_
- _the regular task of updating dishes and categories (in plans)_
- _automatic search for media files (check) in the library with the corresponding mask files during the import of dishes (possibly in plans)_

### What does not do

- - does not receive data from other R_Keeper systems (CRM and others). It is expected that the client will be identified on the server by phone
- does not establish the cost and delivery method, it is necessary, during the setup of the store (woocommerce) to independently configure the methods and cost of delivery
- 


## Installation

Installing "WP R_Keeper" can be done either by searching for "WP R_Keeper" via the "Plugins > Add New" screen in your WordPress dashboard, or by using the following steps:

1. Download the plugin
2. Upload the ZIP file through the 'Plugins > Add New > Upload' screen in your WordPress dashboard
3. Activate the plugin through the 'Plugins' menu in WordPress

## Dependencies

The plugin requires installed and activated WooCommerce older than version 3.0.0,
and also requires the plug-in support for composite products "WPC Composite Products".

If these plugins are not installed or activated, a notification will be displayed.
with the proposal to install the necessary plugins and activate them.

## Initial setup

To get started, you must specify two required parameters received from R_Keeper dealers: `sid` and `objectid`. After these two parameters are set, from the main page of the plugin you can get and select Restaurant and Settlement Account by default.

## R_Keeper info

- Wiki (not ready yet)
- R_Keeper Documentation
  - https://apidocs.ucs.ru/
  - [webdelivery_api](https://apidocs.ucs.ru/doku.php/ru:webdelivery_api)
  - [webdelivery_rk7_api](https://apidocs.ucs.ru/doku.php/ru:webdelivery_rk7_api)
