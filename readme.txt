=== WordPress R_Keeper WebDelivery ===
Contributors: MrSwed
Tags: wordpress, woocommerce, e-commerce, delivery, cafe, restaurant, plugin, template
Requires at least: 5.3
Tested up to: 5.4
Stable tag: trunk.
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html
Url: https://gitlab.com/sdwp/plugins/wp-r-keeper
Bug Tracker: https://gitlab.com/sdwp/plugins/wp-r-keeper/issues  

Плагин WordPress R_Keeper WebDelivery предназначен для автоматизации построения лицевого сайта для ресторана или кафе, работающего с системой [R_Keeper Delivery](https://rkeeper.ru/delivery#rec181756621) на базе cms WordPress.

== Description ==

= Возможности =

- получение списка ресторанов, выбор ресторана по умолчанию для работы с сайтом
- синхронизация блюд и меню выбранного ресторана
- автоматическая привязка изображений к блюдам, во время их загрузки, по заданной маске файла, содержащей артикул блюда (`*0000.*`)
- регулярная задача обновления блюд и категорий
- _регистрация заказов в R_Keeper (в разработке)_
- _автоматический поиск медиафайлов (проверка) в библиотеке с соответствующими маске файлами во время импорта блюд_ (возможно, в планах) 
- _обновление статусов доставки_ (возможно, в планах) 

== Frequently Asked Questions ==

- Какие основные параметры необходимы для начальной настройки
- Нужен `sid` и `corpId`, полученные от дилеера R_Keeper 

== Installation ==

Установка «WP R_Keeper» может быть выполнена либо путем поиска «WP R_Keeper» через экран «Плагины> Добавить новый» на панели инструментов WordPress, либо с помощью следующих шагов:

1. Скачать плагин
2. Загрузите ZIP-файл через экран «Плагины> Добавить новый> Загрузить» на панели инструментов WordPress.
3. Активируйте плагин через меню «Плагины» в WordPress.

== Screenshots ==


== Contributors & Developers ==

Url: https://gitlab.com/sdwp/plugins/wp-r-keeper
Bug Tracker: https://gitlab.com/sdwp/plugins/wp-r-keeper/issues  

== Changelog ==

= 1.0 - 2020-06-05 =

== Upgrade Notice ==

= 1.0 =

